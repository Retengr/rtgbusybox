FROM centos
RUN yum -y update && yum -y install wget && yum -y install git
RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
RUN yum -y install unzip && yum install -y zip
RUN unzip awscliv2.zip
RUN ./aws/install
RUN yum install -y httpd-tools
RUN yum install -y expect # Contains mkpasswd
RUN yum install epel-release -y
RUN yum install -y ansible 
